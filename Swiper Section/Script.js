"use strict";


let mySwiper = new Swiper('.reviews-section-wrapper', {
    slidesPerView: 1,
    loop: true,
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
        renderBullet: function (index, className) {
            return '<span class="' + className + '">' + (index + 1) + '</span>';
        }
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },


});
let swiperSmallElement1 = document.querySelectorAll('.swiper-pagination-bullet')[0];
let swiperSmallElement2 = document.querySelectorAll('.swiper-pagination-bullet')[1];
let swiperSmallElement3 = document.querySelectorAll('.swiper-pagination-bullet')[2];
let swiperSmallElement4 = document.querySelectorAll('.swiper-pagination-bullet')[3];

swiperSmallElement1.innerHTML = '<img class="author-photo-small" src="Images/MaryAvg.jpg" alt="MaryAvg.jpg">';
swiperSmallElement2.innerHTML = '<img class="author-photo-small" src="Images/CoulRo.jpg" alt="CoulRo.jpg">';
swiperSmallElement3.innerHTML = '<img class="author-photo-small" src="Images/AlayzaTeilor.jpg" alt="AlayzaTeilor.jpg">';
swiperSmallElement4.innerHTML = '<img class="author-photo-small" src="Images/BobMorly.jpg" alt="/BobMorly.jpg">';

