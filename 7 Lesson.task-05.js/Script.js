"use strict";
/* ЗАДАЧА 5
* Есть объект склада,
* Написать метод объекта getAmount(goodsList), который принимает в качестве аргумента СТРОКУ СО СПИСКОМ ТОВАРОВ,
* ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ - строка в которой содержится для каждого товара - имя и оставшееся количество на складе.
* В случае если какого-то товара на складе нет в принципе - напротив него написать 'not found'
* Названия товаров не должны быть чувствительны к регистру, т.е. если на складе есть apple,
* то пользователь может ввести Apple или aPPle и получить нужную ему инфомрацию.
*/
const STORAGE = {
    apple: 8,
    beef: 162,
    banana: 14,
    chocolate: 0,
    milk: 2,
    water: 16,
    coffee: 0,
    blackTea: 13,
    cheese: 0,
    getAmount: function (goodList) {
        let resultStr = '';
        goodList = goodList.split(',');
        for (let i = 0; i < goodList.length; i++) {
            for (let key in STORAGE) {
                if (goodList[i] === key) {
                    resultStr += (key + '-' + STORAGE[key] + '\n');
                    break;
                } else if (!STORAGE.hasOwnProperty(goodList[i])) {
                    resultStr += `${goodList[i]}-is not found\n`;
                    break;
                }
            }
        }
        return resultStr;
    }
};
let goodList = prompt('Enter what product\'s is interesting for you?\nPlease write with commas', 'apple,milk').toLowerCase();
console.log(STORAGE.getAmount(goodList));
